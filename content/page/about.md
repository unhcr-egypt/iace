---
title: About Us
subtitle: The IAWG is the highest coordination level used for the refugee response in Egypt
comments: false
---


The IAWG is the highest coordination level used for the refugee response in Egypt. On this level, partners discuss policy issues, protection and programme gaps with regards to refugee communities of all nationalities. The ISWG is an inter-sectorial operational forum which brings together different sector working groups, i.e., protection, public health, education, food security, basic needs and cash-based interventions, livelihoods, and communication with communities.

![Boys](/static/img/iace_boys.jpeg)


The ISWG is mandated to coordinate, identify and evaluate relevant operational topics to ensure a formative and standard approach. The ISWG reports to the IAWG as a higher level of coordination for policy decision and overall guidance. Each sector working group has its specific set of partners including Government ministries, donors, international agencies and international and national NGOs. Under the protection working group, three sub-working groups are established to cover Child Protection, SGBV, and Durable Solutions. The Durable Solutions Sub Working Group (DSSWG) is an interagency working group that focuses on the durable solutions for refugees, primarily voluntary repatriation/return of refugees; and resettlement/complementary pathways for legal admission to third countries. One of the key achievements of this WG is conducting periodic intention surveys (through questionnaires and focus group discussions) to measure and analyze perceptions of Syrian refugees on return.

[UNHCR](https://www.unhcr.org/eg), as chair of the IAWG, briefs the Resident Coordinator and the United Nations Country Team (UNCT) on developments on refugee issues and provides updates on 3RP coordination on a regular basis. [The United Nations Development Programme (UNDP)](https://www.eg.undp.org/) facilitates the coordination regarding the resilience component.
